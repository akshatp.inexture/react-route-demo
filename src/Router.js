import { Routes, Route } from "react-router-dom";
import React from "react";
import About from "./component/About";
import Contact from "./component/Contact";
import Shop from "./component/Shop";
import Home from "./component/Home";
import Login from "./component/Login";
import Profile from "./component/Profile";
import Logout from "./component/Logout";
import PrivateRoute from "./component/PrivateRoute";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/contact" element={<Contact />} />
      {/* <Route path="/shop" element={<Shop />} /> */}
      <Route path="/login" element={<Login />} />
      <Route element={<PrivateRoute />}>
        <Route path="/profile" element={<Profile />}></Route>
        <Route path="/profile/:ID" element={<Shop />}></Route>
      </Route>
      <Route path="/logout" element={<Logout />}></Route>

      <Route path="*" element={<h1>Page not found</h1>}></Route>
    </Routes>
  );
};

export default Router;
