import "./App.css";
import Router from "./Router";
import Nav from "./component/Nav";
import DemoState from "./context/DemoState";
function App() {
  return (
    <>
      <DemoState>
        <Nav />
        <Router />
      </DemoState>
    </>
  );
}

export default App;
