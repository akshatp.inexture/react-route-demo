import React, { useReducer } from "react";
import demoContext from "./demoContext";
import demoReducer from "./DemoReducer";

const DemoState = (props) => {
  const initialState = {
    name: localStorage.getItem("state")
      ? JSON.parse(localStorage.getItem("state")).firstName
      : "",
    lastname: localStorage.getItem("state")
      ? JSON.parse(localStorage.getItem("state")).lastName
      : "",
    isAuth: localStorage.getItem("state")
      ? JSON.parse(localStorage.getItem("state")).isAuth
      : false,
  };
  const [state, dispatch] = useReducer(demoReducer, initialState);
  const changeName = (value) => {
    dispatch({
      type: "ChangeName",
      payload: value,
    });
  };
  const logout = () => {
    dispatch({
      type: "Logout",
    });
  };
  return (
    <demoContext.Provider
      value={{
        name: state.name,
        lastname: state.lastname,
        isAuth: state.isAuth,
        changeName,
        logout,
      }}
    >
      {props.children}
    </demoContext.Provider>
  );
};

export default DemoState;
