const DemoReducer = (state, action) => {
  switch (action.type) {
    case "ChangeName":
      localStorage.setItem(
        "state",
        JSON.stringify({ ...action.payload, isAuth: true })
      );
      return {
        name: action.payload.firstName,
        lastname: action.payload.lastName,
        isAuth: true,
      };
    case "Logout":
      localStorage.removeItem("state");
      return {
        name: "",
        lastName: "",
        isAuth: false,
      };
    default:
      return state;
  }
};

export default DemoReducer;
