import { createContext, useContext } from "react";

const demoContext = createContext();

export default demoContext;

export const useDemoContext = () => useContext(demoContext);
