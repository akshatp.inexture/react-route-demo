import React, { useContext } from "react";
import { Link } from "react-router-dom";
import demoContext from "../context/demoContext";

const Nav = () => {
  const DemoContext = useContext(demoContext);
  const { isAuth } = DemoContext;
  var asd = 10;
  return (
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
        <li>
          <Link to={`/profile/${asd}`} state="manan">
            Shop
          </Link>
        </li>
        <li>
          <Link to="/contact">Contacts</Link>
        </li>
        {!isAuth ? (
          <li>
            <Link to="/login">Login</Link>
          </li>
        ) : (
          <li>
            <Link to="/logout">logout</Link>
          </li>
        )}
        <li>
          <Link to="/profile">Profile</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
