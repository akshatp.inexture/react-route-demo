import React, { useState, useContext, useEffect } from "react";
import demoContext, { useDemoContext } from "../context/demoContext";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const [input, setInput] = useState({
    firstName: "",
    lastName: "",
  });
  // const DemoContext = useContext(demoContext);
  const DemoContext = useDemoContext();
  const { changeName, isAuth } = DemoContext;

  const navigate = useNavigate();

  useEffect(() => {
    if (isAuth) {
      navigate("/profile");
    }
  }, [isAuth]);

  const history = () => {
    navigate("/");
  };

  const handleSubmit = () => {
    if (input.firstName === "" || input.lastName === "") {
      window.alert("Enter Name And Last name");
    } else {
      changeName(input);
    }
  };
  return (
    <div>
      <h2> Login Page</h2>
      <input
        type="text"
        value={input.firstName}
        name="firstName"
        onChange={(e) =>
          setInput({ ...input, [e.target.name]: e.target.value })
        }
      ></input>
      <input
        type="text"
        value={input.lastName}
        name="lastName"
        onChange={(e) =>
          setInput({ ...input, [e.target.name]: e.target.value })
        }
      ></input>
      <button onClick={handleSubmit}>Sign in</button>
      <button onClick={history}>Go back</button>
    </div>
  );
};

export default Login;
