import React, { useContext } from "react";
import demoContext from "../context/demoContext";

const Profile = () => {
  const DemoContext = useContext(demoContext);
  const { name } = DemoContext;
  return <div>{name}</div>;
};

export default Profile;
