import React, { useContext } from "react";
import { Outlet, Navigate } from "react-router-dom";
import demoContext from "../context/demoContext";

const PrivateRoute = () => {
  const DemoContext = useContext(demoContext);
  const { isAuth } = DemoContext;
  if (!isAuth) {
    return <Navigate to="/login" />;
  }
  return <Outlet />;
};

export default PrivateRoute;
