import React, { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import demoContext from "../context/demoContext";

const Logout = () => {
  const DemoContext = useContext(demoContext);
  const { logout, isAuth } = DemoContext;
  const navigate = useNavigate();
  useEffect(() => {
    if (!isAuth) {
      navigate("/login");
    }
  }, [isAuth]);

  return (
    <div>
      <button onClick={logout}>Logout</button>
    </div>
  );
};

export default Logout;
